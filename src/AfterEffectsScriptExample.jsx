Array.prototype.map = function(callback) {
    var arr = [];
    for (var i = 0; i < this.length; i++) {
        arr.push(callback(this[i]));
    }
    return arr;
}
Array.prototype.forEach = function (callback) {
    for (var i = 0; i < this.length; i++) {
        callback(this[i], i);
    }
}
Array.prototype.indexOf = function (val) {
    for (var i = 0; i < this.length; i++) {
        if(this[i] === val) {
            return i;
        }
    }
    return -1;
}
Array.prototype.transpose = function() {
    var arr = [];
    for (var i = 0; i < this.length; i++) {
        if (this[i] instanceof Array) {
            for (var j = 0; j < this[i].length; j++) {
                if (!(arr[j] instanceof Array)) {
                    arr[j] = [];
                }
                arr[j][i] = this[i][j];
            }
        }
    }
    return arr;
}

{
    var Text = {
        iface: {
            'window-title': 'Titres',
            'data-panel-title': 'Импорт титров',
            'data-panel-placeholder': 'Вставте титры сюда',
            'data-panel-apply': 'Создать титры',
            'create-titres': 'Создание Титров',
            'text-color-panel-title': 'Выбор цвета выделения текста',
            colors: {
                orange: "Оранжевый",
                gray: "Серый"
            }
        },
        error: {
            'no-model': 'В проекте отсутствует макет "<model>"',
        }
    };
    var Utils = {
        getDataFromString: function(str) {
            str = str
                .replace(/\n +\n/g, "\n\n")
                .replace(/(^"|"+$)/g, "")
                .replace(/<br>/g, "\r")
                .replace(/(^\n|^ |\n+$| +$)/g, "")
                .split("\n\n");
            var data = [];
            for (var i = 0; i < str.length; i++) {
                str[i] = str[i].replace(/(|^ | +$)/g, "");
                var type = str[i].split(' ')[0];
                type = type[0] === '#' ? type.slice(1) : 'title';
                if (type !== 'break') {
                    data.push({
                        type: type,
                        text: str[i].replace('#' + type + ' ', "")
                    })
                }
            }
            return data;
        },
        getItem: function(name, type) {
            var doc = app.project;
            for (var i = 1; i <= doc.numItems; i++) {
                if (doc.item(i).name === name) {
                    if (type) {
                        if (doc.item(i) instanceof type) {
                            return doc.item(i);
                        }
                    } else {
                        return doc.item(i);
                    }
                }
            }
            return null;
        },
        getOutputFolder: function() {
            return Utils.getItem('OutputTitres', FolderItem) || app.project.items.addFolder('OutputTitres');
        },
        setText: function(layer, text) {
            var property = layer.text.property("Source Text");
            var value = property.value;
            value.text = text;
            property.setValue(value);
        },
        colorizeText: function (layer, data) {
            for (var i = 0; i < 100; i++) {
                var anim = layer.property("ADBE Text Properties").property(4).property("Animator " + (i + 1));
                if (anim) {
                    anim.remove();
                } else {
                    break;
                }
            }
            try {
                var selectArr = Utils.getSelectArr(data);
                selectArr.forEach(function (select){
                    Utils.colorSelectText(layer, select);
                });
            } catch (e) {

            }
        },
        getSelectArr: function(str) {
            var arr = [];
            var s = str;
            for (var i = 0; i < str.match(/</g).length; i++) {
                var startIndex = s.indexOf('<');
                var endIndex = s.indexOf('>') - 1;

                if(endIndex < 0) {
                    endIndex = s.length - 1;
                }
                if (startIndex >= 0) {
                    var controlStart = s.substr(0, startIndex).match(/\n/g);
                    controlStart = controlStart ? controlStart.length : 0;
                    var controlEnd = s.substr(0, endIndex).match(/\n/g);
                    controlEnd = controlEnd ? controlEnd.length : 0;
                    arr.push({start: startIndex - controlStart, end: endIndex - controlEnd});
                    s = s.replace('<', '').replace('>', '');
                }
            }

            return arr;
        },
        colorSelectText: function(textLayer, select) {
            var grpTextAnimators = textLayer.property("ADBE Text Properties").property(4);
            var grpTextAnimator = grpTextAnimators.addProperty("ADBE Text Animator");
            var textSelector = grpTextAnimator.property(1).addProperty("ADBE Text Selector");
            textSelector.property(7).property("ADBE Text Range Units").setValue(2);
            textSelector.property("ADBE Text Index Start").setValue(select.start);
            textSelector.property("ADBE Text Index End").setValue(select.end);
            grpTextAnimator.property("ADBE Text Animator Properties").addProperty("ADBE Text Fill Color").expression =
                'comp("Model_title_4x5").layer("reference_text").text.animator("Animator 1").property.fillColor';
        },
        getID: function(num, idLength) {
            idLength = idLength || 1;
            var orderNum = Math.pow(10, idLength);

            if (num < orderNum) {
                var numLength = num.toString().length;
                var deffLength = orderNum.toString().length - numLength;
                return orderNum.toString().substr(1, deffLength - 1) + num;
            } else {
                return num.toString();
            }
        },
        hexToRgb: function(hex) {
            var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
            return result
                ? [ parseInt(result[1], 16), parseInt(result[2], 16), parseInt(result[3], 16) ]
                : null;
        }
    };
    var Interface = {
        getDropdown: function (parent, list, text, onChangeEventName, selection) {
            var group = parent.add("group");
            group.margins = 15;

            if (text) {
                group.add('statictext', undefined, text);
            }

            var dd = group.add("dropdownlist");
            dd.itemSize = [150,20];
            for(var i = 0; i < list.length; i++) {
                var item = dd.add("item", list[i].text);
                item.name = list[i].name;
            }
            dd.selection = selection || 0;

            dd.onChange = function(){
                group.onChange({type: onChangeEventName || 'dropdown-on-change', name: this.selection.name, text: this.selection.text});
            };

            return group;
        },
        getColorDropdown: function (parent, colorList, text, onChangeEventName, selection) {
            var PngSwatch = (function(){
                var CRC_256 = [0, 0x77073096, 0xee0e612c, 0x990951ba, 0x76dc419, 0x706af48f, 0xe963a535, 0x9e6495a3, 0xedb8832, 0x79dcb8a4, 0xe0d5e91e, 0x97d2d988, 0x9b64c2b, 0x7eb17cbd, 0xe7b82d07, 0x90bf1d91, 0x1db71064, 0x6ab020f2, 0xf3b97148, 0x84be41de, 0x1adad47d, 0x6ddde4eb, 0xf4d4b551, 0x83d385c7, 0x136c9856, 0x646ba8c0, 0xfd62f97a, 0x8a65c9ec, 0x14015c4f, 0x63066cd9, 0xfa0f3d63, 0x8d080df5, 0x3b6e20c8, 0x4c69105e, 0xd56041e4, 0xa2677172, 0x3c03e4d1, 0x4b04d447, 0xd20d85fd, 0xa50ab56b, 0x35b5a8fa, 0x42b2986c, 0xdbbbc9d6, 0xacbcf940, 0x32d86ce3, 0x45df5c75, 0xdcd60dcf, 0xabd13d59, 0x26d930ac, 0x51de003a, 0xc8d75180, 0xbfd06116, 0x21b4f4b5, 0x56b3c423, 0xcfba9599, 0xb8bda50f, 0x2802b89e, 0x5f058808, 0xc60cd9b2, 0xb10be924, 0x2f6f7c87, 0x58684c11, 0xc1611dab, 0xb6662d3d, 0x76dc4190, 0x1db7106, 0x98d220bc, 0xefd5102a, 0x71b18589, 0x6b6b51f, 0x9fbfe4a5, 0xe8b8d433, 0x7807c9a2, 0xf00f934, 0x9609a88e, 0xe10e9818, 0x7f6a0dbb, 0x86d3d2d, 0x91646c97, 0xe6635c01, 0x6b6b51f4, 0x1c6c6162, 0x856530d8, 0xf262004e, 0x6c0695ed, 0x1b01a57b, 0x8208f4c1, 0xf50fc457, 0x65b0d9c6, 0x12b7e950, 0x8bbeb8ea, 0xfcb9887c, 0x62dd1ddf, 0x15da2d49, 0x8cd37cf3, 0xfbd44c65, 0x4db26158, 0x3ab551ce, 0xa3bc0074, 0xd4bb30e2, 0x4adfa541, 0x3dd895d7, 0xa4d1c46d, 0xd3d6f4fb, 0x4369e96a, 0x346ed9fc, 0xad678846, 0xda60b8d0, 0x44042d73, 0x33031de5, 0xaa0a4c5f, 0xdd0d7cc9, 0x5005713c, 0x270241aa, 0xbe0b1010, 0xc90c2086, 0x5768b525, 0x206f85b3, 0xb966d409, 0xce61e49f, 0x5edef90e, 0x29d9c998, 0xb0d09822, 0xc7d7a8b4, 0x59b33d17, 0x2eb40d81, 0xb7bd5c3b, 0xc0ba6cad, 0xedb88320, 0x9abfb3b6, 0x3b6e20c, 0x74b1d29a, 0xead54739, 0x9dd277af, 0x4db2615, 0x73dc1683, 0xe3630b12, 0x94643b84, 0xd6d6a3e, 0x7a6a5aa8, 0xe40ecf0b, 0x9309ff9d, 0xa00ae27, 0x7d079eb1, 0xf00f9344, 0x8708a3d2, 0x1e01f268, 0x6906c2fe, 0xf762575d, 0x806567cb, 0x196c3671, 0x6e6b06e7, 0xfed41b76, 0x89d32be0, 0x10da7a5a, 0x67dd4acc, 0xf9b9df6f, 0x8ebeeff9, 0x17b7be43, 0x60b08ed5, 0xd6d6a3e8, 0xa1d1937e, 0x38d8c2c4, 0x4fdff252, 0xd1bb67f1, 0xa6bc5767, 0x3fb506dd, 0x48b2364b, 0xd80d2bda, 0xaf0a1b4c, 0x36034af6, 0x41047a60, 0xdf60efc3, 0xa867df55, 0x316e8eef, 0x4669be79, 0xcb61b38c, 0xbc66831a, 0x256fd2a0, 0x5268e236, 0xcc0c7795, 0xbb0b4703, 0x220216b9, 0x5505262f, 0xc5ba3bbe, 0xb2bd0b28, 0x2bb45a92, 0x5cb36a04, 0xc2d7ffa7, 0xb5d0cf31, 0x2cd99e8b, 0x5bdeae1d, 0x9b64c2b0, 0xec63f226, 0x756aa39c, 0x26d930a, 0x9c0906a9, 0xeb0e363f, 0x72076785, 0x5005713, 0x95bf4a82, 0xe2b87a14, 0x7bb12bae, 0xcb61b38, 0x92d28e9b, 0xe5d5be0d, 0x7cdcefb7, 0xbdbdf21, 0x86d3d2d4, 0xf1d4e242, 0x68ddb3f8, 0x1fda836e, 0x81be16cd, 0xf6b9265b, 0x6fb077e1, 0x18b74777, 0x88085ae6, 0xff0f6a70, 0x66063bca, 0x11010b5c, 0x8f659eff, 0xf862ae69, 0x616bffd3, 0x166ccf45, 0xa00ae278, 0xd70dd2ee, 0x4e048354, 0x3903b3c2, 0xa7672661, 0xd06016f7, 0x4969474d, 0x3e6e77db, 0xaed16a4a, 0xd9d65adc, 0x40df0b66, 0x37d83bf0, 0xa9bcae53, 0xdebb9ec5, 0x47b2cf7f, 0x30b5ffe9, 0xbdbdf21c, 0xcabac28a, 0x53b39330, 0x24b4a3a6, 0xbad03605, 0xcdd70693, 0x54de5729, 0x23d967bf, 0xb3667a2e, 0xc4614ab8, 0x5d681b02, 0x2a6f2b94, 0xb40bbe37, 0xc30c8ea1, 0x5a05df1b, 0x2d02ef8d];
                var crc32 = function(/*uint[]*/buf){
                    var c = 0xffffffff >>> 0,
                        n = buf.length >>> 0,
                        i;
                    for( i=0 ; i < n ; ++i )
                        c = CRC_256[( ( c>>>0 ) ^ buf[i]) & 0xff] ^ (c >>> 8);

                    return (c ^ 0xffffffff)>>>0;
                };

                var PNG_PROLOG = "\x89PNG\r\n\x1A\n\x00\x00\x00\rIHDR\x00\x00\x00\r\x00\x00\x00\r\b\x03\x00\x00\x00E5\x14N\x00\x00\x00\x06",
                    PNG_EPILOG = "\x00\x00\x00\x16IDATx\xDAb`D\x06\f\x8C\f\b0\xC8x\xC8\x00 \xC0\x00\x11\xC6\x001{U\x93\xB6\x00\x00\x00\x00IEND\xAEB`\x82";

                return function(/*uint[3]*/rgb) {
                    var buf = [0x50,0x4C,0x54,0x45, rgb[0], rgb[1], rgb[2], 0, 0, 0],
                        crc = crc32(buf),
                        i, r;

                    buf = buf.concat([ (crc>>>24)&0xFF, (crc>>>16)&0xFF, (crc>>>8)&0xFF, (crc>>>0)&0xFF ]);
                    i = buf.length;
                    while( i-- )
                        buf[i] = String.fromCharCode(buf[i]);

                    r = PNG_PROLOG + buf.join('') + PNG_EPILOG;

                    buf.length = 0;
                    buf = null;
                    return r;
                };
            })();

            var group = parent.add("group");
            group.margins = 15;

            if (text) {
                group.add('statictext', undefined, text);
            }

            var dd = group.add("dropdownlist");
            dd.itemSize = [150,20];
            colorList.forEach(function(color) {
                var item = dd.add("item", Text.iface.colors[color.name]);
                item.name = color.name;
                item.image = PngSwatch(Utils.hexToRgb(color.hex));
            });
            dd.selection = selection || 0;

            dd.onChange = function(){
                group.onChange({type: onChangeEventName || 'dropdown-on-change', name: this.selection.name, text: this.selection.text});
            };

            return group;
        },
        getEditText: function (parent, placeholder, bounds) {
            var text = parent.add('edittext', bounds, placeholder, {multiline: true});
            parent.placeholder = placeholder || "";
            text.onActivate = function() {
                if(this.text === parent.placeholder)
                    this.text = "";
            };
            text.onDeactivate = function() {
                if(this.text === "" || this.text === " ")
                    this.text = parent.placeholder;
            };
            return text;
        },
        getButton: function (parent, type, bounds) {
            var btn = parent.add('button', bounds, Text.iface[type] || type);
            btn.onClick = function () {
                if (typeof btn.onChange === 'function') {
                    btn.onChange({type: type});
                }
            }
            return btn;
        },
        getDataPanel: function (parent) {
            var panel = parent.add('panel', undefined, Text.iface['data-panel-title']);
            var text = Interface.getEditText(panel, Text.iface['data-panel-placeholder'], [0, 0, 300, 300]);
            var button = Interface.getButton(panel, 'data-panel-apply', [0, 0, 150, 30]);
            button.onChange = function () {
                panel.onChange({ type: 'change-data', data: text.text });
            }
            return panel;
        },
        getTextColorPanel: function(parent) {
            var panel = parent.add('panel', undefined, Text.iface['text-color-panel-title'])
            return Interface.getColorDropdown(
                panel,
                Storage.selectColors,
                null,
                'change-select-color',
                0
            );
        }
    };
    var TitresFactory = {
        title: function (scene, model, data, name) {
            var layer = TitresFactory.textTitre(scene, model, data.replace(/[<>]/g, ""), name);
            Utils.colorizeText(layer, data);
            return layer;
        },
        syn: function (scene, model, data, name) {
            return TitresFactory.textTitre(scene, model, data, name);
        },
        copy: function (scene, model, data, name) {
            return TitresFactory.compTitre(scene, model, data, name);
        },
        geo: function (scene, model, data, name) {
            return TitresFactory.compTitre(scene, model, data, name);
        },
        person: function (scene, model, data, name) {
            var layer = TitresFactory.compTitre(scene, model, data.replace(/[<>]/g, ""), name);
            Utils.colorizeText(layer.source.layers.byName('name'), data);
            return layer;
        },
        textTitre: function(scene, model, data, name) {
            model.layers.byName('reference_text').copyToComp(scene);
            var layer = scene.layers.byName('reference_text');
            layer.name = name;
            Utils.setText(layer, data);
            return layer;
        },
        compTitre: function (scene, model, data, name) {
            var titreComp = model.duplicate();
            titreComp.name = name;
            titreComp.parentFolder = Utils.getOutputFolder();

            var titerLayer = scene.layers.add(titreComp);
            titerLayer.name = name;

            Utils.setText(titreComp.layers.byName('reference_text'), data);

            return titerLayer;
        }
    };
    var Storage = {
        comps: [
            { name: '4x5', width: 1080, height: 1350 }
        ],
        models: {
            title: {
                label: 7
            },
            syn: {
                label: 4
            },
            copy: {
                label: 2
            },
            geo: {
                label: 8
            },
            person: {
                label: 10
            },
        },
        selectColors: [{
            name: 'orange',
            hex: '#FA6714',
            rgb: [250/255, 103/255, 20/255]
        }, {
            name: 'gray',
            hex: '#7D7D7D',
            rgb: [125/255,125/255,125/255]
        },
        ],
        selectColor: {
            name: 'orange',
            rgb: [250/255, 103/255, 20/255],
            hex: '#FA6714'
        }
    };

    function createTitres(data) {
        Storage.comps.forEach(function (options) {
            var sceneName = 'Scene_' + options.name;
            var scene = Utils.getItem(sceneName, CompItem) ||
                app.project.items.addComp(sceneName, options.width, options.height, 1, 600, 25);
            data.forEach(function (d, index) {
                createTitre(scene, d, options.name, index);
            })
        });
    }
    function createTitre(scene, data, prefix, index) {
        var modelName = 'Model_' + data.type + '_' + prefix;
        var model =  Utils.getItem(modelName, CompItem);

        if (!model) {
            alert(Text.error['no-model'].replace('<model>', modelName));
            return null;
        }

        var titreName = 'Titre-' + prefix + '-' + index;
        var titreComp = Utils.getItem(titreName, CompItem);

        if (titreComp) {
            titreComp.remove();
        }

        var layer = TitresFactory[data.type](scene, model, data.text, titreName);
        layer.label = Storage.models[data.type].label;
        layer.startTime = index * 5;
        layer.outPoint = (index + 1) * 5;
        layer.moveToBeginning();
    }
    function changeSelectColor(rgb) {
        Storage.comps.forEach(function (scene) {
            var titreComp = Utils.getItem('Model_title_' + scene.name, CompItem)
            var textLayer = titreComp.layers.byName('reference_text');
            var grpTextAnimators = textLayer.property("ADBE Text Properties").property(4);
            var grpTextAnimator = grpTextAnimators.property("Animator 1");
            grpTextAnimator.property("ADBE Text Animator Properties").addProperty("ADBE Text Fill Color").setValue(rgb);
        })
    }

    (function init() {
        var win = (this instanceof Panel) ? this : new Window('palette', Text.iface['window-title']);
        var group = win.add('group');
        group.orientation = 'column';
        group.alignChildren = 'center';

        var dataPanel = Interface.getDataPanel(group);

        var textColorPanel = Interface.getTextColorPanel(group);

        dataPanel.onChange =
        textColorPanel.onChange =
            eventHandler;

        win.layout.layout(true);
        group.minimumSize = group.size;
        win.layout.resize();
        win.onResizing = win.onResize = function () {this.layout.resize();}

        if (win instanceof Window) {
            win.show();
        }

        function eventHandler(event) {
            switch(event.type) {
                case 'change-data':
                    try {
                        app.beginUndoGroup(Text.iface['create-titres']);
                        createTitres(Utils.getDataFromString(event.data));
                        app.endUndoGroup();
                    } catch (e) {
                        alert(e);
                    }
                    break;
                case 'change-select-color':
                    try {
                        changeSelectColor(Storage.selectColors.filter(function(sc) {
                            return sc.name === event.name;
                        })[0].rgb);
                    } catch (e) {
                        alert(e);
                    }
                    break;
            }
        }
    })();
}