import React, { useLayoutEffect, useState, useRef } from 'react'
import S from './module.css'
import playSvg from 'static/assets/btn-play.svg'
import pauseSvg from 'static/assets/btn-pause.svg'
import html from 'react-inner-html'
import gsap from 'gsap'

const commands = ['play', 'pause']
const btnPlaySvg = [playSvg, pauseSvg]
let controlClear = false
let firstShow = true
let tl, startX, offset

export default function Playbar({ values, onChange, clear }) {
  const [nextCommandId, setNextCommandId] = useState(0)
  const [value, setValue] = useState(values[0])
  const [width, setWidth] = useState(0)
  const [yearsStep, setYearsStep] = useState(0)
  const tlRef = useRef(null)

  useLayoutEffect(() => {
    if (!tl) {
      tl = gsap.to(
        {
          i: 0,
        },
        {
          duration: 10,
          i: 1,
          onUpdate: onUpdateTl,
          onComplete: onCompleteTl,
        }
      )
      tl.pause()
    }

    if (firstShow) {
      window.addEventListener('scroll', startProject)
      startProject()
      return () => window.removeEventListener('scroll', startProject)
    }
  })

  useLayoutEffect(() => {
    window.addEventListener('resize', resize)
    resize()
    return () => window.removeEventListener('resize', resize)
  })

  const resize = () => {
    setYearsStep(tlRef.current.clientWidth / (values.length - 1))
  }

  const startProject = () => {
    if (firstShow) {
      const y = tlRef.current.getBoundingClientRect().top
      if (y < window.innerHeight && y > 0) {
        firstShow = false
        tl.play()
        setNextCommandId(1)
      }
    }
  }

  const onCompleteTl = () => {
    setNextCommandId(Math.abs(nextCommandId))
  }

  const onUpdateTl = () => {
    const progress = tl.progress()
    setWidth(Math.floor(progress * 100))
    setValue(getValue(progress))

    onChange(progress)
  }

  const onClickBtnPlay = () => {
    const progress = tl.progress()
    if (progress === 1) {
      tl.progress(0)
    }
    tl[commands[nextCommandId]]()
    setNextCommandId(Math.abs(nextCommandId - 1))
  }

  const checkClear = () => {
    if (controlClear !== clear) {
      controlClear = clear
      setNextCommandId(1)
      tl.play(0)
    }
  }

  const getValue = progress =>
    values[Math.floor((values.length - 1) * progress)]

  const tlEventHandler = e => {
    switch (e.type) {
      case 'mousedown':
        if (typeof startX !== 'number') {
          tl.pause()
          setNextCommandId(0)
          startX = e.pageX
          tlRef.current.querySelector('div').style.transition = 'none'
          document.addEventListener('mousemove', tlEventHandler)
          document.addEventListener('mouseup', tlEventHandler)
          document.addEventListener('mouseleave', tlEventHandler)
        }
        break
      case 'mousemove':
        if (typeof startX !== 'number') {
          document.removeEventListener('mousemove', tlEventHandler)
          document.removeEventListener('mouseup', tlEventHandler)
          document.removeEventListener('mouseleave', tlEventHandler)
          break
        }

        offset = tl.progress() + (e.pageX - startX) / tlRef.current.clientWidth
        tl.progress(offset < 0 ? 0 : offset > 1 ? 1 : offset)
        startX = e.pageX
        break
      case 'mouseup':
      case 'mouseleave':
        tlRef.current.querySelector('div').style.transition = ''
        document.removeEventListener('mousemove', tlEventHandler)
        document.removeEventListener('mouseup', tlEventHandler)
        document.removeEventListener('mouseleave', tlEventHandler)
        startX = null
        break
      case 'touchstart':
        e.preventDefault()
        e.stopPropagation()
        if (typeof startX !== 'number') {
          tl.pause()
          setNextCommandId(0)
          startX = e.touches[0].pageX
          tlRef.current.querySelector('div').style.transition = 'none'
          document.addEventListener('touchmove', tlEventHandler)
          document.addEventListener('touchend', tlEventHandler)
        }
        break
      case 'touchmove':
        if (typeof startX !== 'number') {
          document.removeEventListener('touchmove', tlEventHandler)
          document.removeEventListener('touchend', tlEventHandler)
          break
        }

        e.preventDefault()
        offset =
          tl.progress() +
          (e.touches[0].pageX - startX) / tlRef.current.clientWidth
        tl.progress(offset < 0 ? 0 : offset > 1 ? 1 : offset)
        startX = e.touches[0].pageX
        break
      case 'touchend':
        tlRef.current.querySelector('div').style.transition = ''
        document.removeEventListener('touchmove', tlEventHandler)
        document.removeEventListener('touchend', tlEventHandler)
        startX = null
        break
    }
  }

  checkClear()

  return (
    <div className={S.playbar}>
      <div
        className={S['btn-play']}
        onClick={onClickBtnPlay}
        {...html(btnPlaySvg[nextCommandId])}
      />
      <div
        className={S.timeline}
        onMouseDown={tlEventHandler}
        onTouchStart={tlEventHandler}
        onClick={tlEventHandler}
      >
        <div className={S['timeline-bg']} ref={tlRef}>
          <div className={S['timeline-todd']} style={{ width: `${width}%` }} />
          {values.slice(2).map((_, i) => (
            <div
              key={i}
              className={S.year}
              style={{ left: `${(i + 1) * yearsStep - 2}px` }}
            />
          ))}
        </div>
      </div>
      <div className={S.value} {...html(value)} />
    </div>
  )
}
