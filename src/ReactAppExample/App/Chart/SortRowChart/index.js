import React from 'react'
import S from './module.css'
import html from 'react-inner-html'

export default function SortRowChart({ data, progress }) {
  const curentValId = Math.floor(
    (Object.values(data[0].data).length - 1) * progress
  )
  const D = data.slice()
  D.sort(
    (a, b) =>
      Object.values(b.data)[curentValId] - Object.values(a.data)[curentValId]
  ).forEach((d, i) => (d.position = i))
  const maxValue = Object.values(D[0].data)[curentValId]

  return (
    <div className={S['sort-row-chart']}>
      {data.map(d => {
        const value = Object.values(d.data)[curentValId]
        const width = Math.ceil(60 * (value / maxValue))
        return (
          <div
            className={S.row}
            key={d.country}
            data-russia={d.country === 'Россия'}
            style={{
              opacity: +(d.position < 7),
              transform: `translateY(${d.position * 42}px)`,
            }}
          >
            <div className={S.name} {...html(d.country)} />
            <div className={S.column} style={{ width: `${width}%` }} />
            <div className={S.value} {...html(value || '')} />
          </div>
        )
      })}
    </div>
  )
}
