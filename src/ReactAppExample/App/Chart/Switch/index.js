import React, { useState } from 'react'
import S from './module.css'
import html from 'react-inner-html'

export default function Switch({ text, onChange }) {
  const [active, setActive] = useState(0)

  return (
    <div className={S.switch}>
      {[0, 1].map(s => (
        <div
          key={s}
          className={S.button}
          data-active={s === active}
          onClick={() => {
            setActive(s)
            onChange({ activate: s })
          }}
          {...html(text[`switch-${s}`])}
        />
      ))}
    </div>
  )
}
