import React, { useState } from 'react'
import S from './module.css'
import Switch from './Switch'
import SortRowChart from './SortRowChart'
import Playbar from './Playbar'

const chartTypes = ['all', 'gold']

export default function Chart({ text, data }) {
  const [chartType, setChartType] = useState(chartTypes[0])
  const [clearPlaybar, setClearPlaybar] = useState(false)
  const [progress, setProgress] = useState(false)

  const changeChart = e => {
    setChartType(chartTypes[e.activate])
    setClearPlaybar(!clearPlaybar)
  }

  const onProgress = progress => {
    setProgress(progress)
  }

  return (
    <div className={S.chart}>
      <Switch text={text} onChange={changeChart} />
      <SortRowChart text={text} data={data[chartType]} progress={progress} />
      <Playbar
        text={text}
        onChange={onProgress}
        values={Object.keys(data[chartType][0].data)}
        clear={clearPlaybar}
      />
    </div>
  )
}
