import React from 'react'
import S from './module.css'
import T from 'static/text.json'
import html from 'react-inner-html'

export default function Footer() {
  return (
    <div className={S.footer}>
      <div className={S.source} {...html(T.source)} />
      <div className={S.copy} {...html(T.copy)} />
    </div>
  )
}
