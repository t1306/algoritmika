import React from 'react'
import html from 'react-inner-html'
import S from './module.css'

export default function Header({ text }) {
  return (
    <div className={S.header}>
      <div className={S.title} {...html(text.title)} />
      <div className={S.lead} {...html(text.lead)} />
    </div>
  )
}
