import React from 'react'
import S from './module.css'
import Header from './Header'
import Footer from './Footer'
import Chart from './Chart'
import text from 'static/text.json'

export default function App(props) {
  return (
    <div className={S.app}>
      <Header text={text} />
      <Chart {...props} text={text} />
      <Footer />
    </div>
  )
}
