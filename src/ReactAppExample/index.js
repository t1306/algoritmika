import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import config from '../project.json'
import hash from 'utils/hash'

fetch(`${config.url.dc}data.json?${hash()}`)
  .then(data => data.json())
  .then(data => {
    ReactDOM.render(<App data={data} />, document.getElementById(config.name))
  })
  .catch(() => {
    fetch(`static/data.json`)
      .then(data => data.json())
      .then(data => {
        ReactDOM.render(
          <App data={data} />,
          document.getElementById(config.name)
        )
      })
  })
