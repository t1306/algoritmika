export default function randomhash() {
  return (~~(Math.random() * 100000000)).toString(16)
}
