import XLSX from 'xlsx'
import fs from 'fs'

const xlsxData = XLSX.readFile('materials/data.xlsx')
const listNames = ['all', 'gold']
const years = ['2020',	'2016',	'2012',	'2008',	'2004',	'2000',	'1996',	'1992'];
const columns = ['B', 'C', 'D', 'E', 'F', 'G', 'H', 'I']
const countreis = {
    all: [
      'Россия',
      'Китай',
      'США',
      'Великобритания',
      'Германия',
      'Австралия',
      'Франция',
      'Япония',
      'Республика Корея',
      'Италия',
      'Куба',
      'Венгрия'
    ],
    gold: [
      'Россия',
      'Китай',
      'США',
      'Великобритания',
      'Германия',
      'Австралия',
      'Франция',
      'Япония',
      'Республика Корея',
      'Италия',
      'Куба',
      'Испания',
      'Нидерланды',
    ]
}
const obj = {}

for (var i = 0; i < listNames.length; i++) {
  const sheet = xlsxData.Sheets[listNames[i]]
  const data = [];

  for(var j = 0; j < countreis[listNames[i]].length; j++) {
    const country = countreis[listNames[i]][j];
    let countryData = { country, data: {} }
    data.push(countryData)
    for (var l = 0; l < years.length; l++) {
      countryData.data[years[l]] = sheet[`${columns[l]}${j+2}`] !== undefined ? +sheet[`${columns[l]}${j+2}`].w : 0
    }
  }

  obj[listNames[i]] = data;
}

var json = JSON.stringify(obj);
fs.writeFileSync('static/data.json', json)
