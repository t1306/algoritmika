import 'core-js/features/promise'
import 'core-js/features/object/keys'
import 'whatwg-fetch'

const polyfills = {
  symbol: import(/* webpackChunkName: "symbol" */ 'core-js/features/symbol'),

  from: import(
    /* webpackChunkName: "array-from" */ 'core-js/features/array/from'
  ),
  find: import(
    /* webpackChunkName: "array-find" */ 'core-js/features/array/find'
  ),
  findIndex: import(
    /* webpackChunkName: "array-find-index" */ 'core-js/features/array/find-index'
  ),
  includes: import(
    /* webpackChunkName: "array-includes" */ 'core-js/features/array/includes'
  ),
  assign: import(
    /* webpackChunkName: "object-assign" */ 'core-js/features/object/assign'
  ),
  values: import(
    /* webpackChunkName: "object-values" */ 'core-js/features/object/values'
  ),
  entries: import(
    /* webpackChunkName: "object-entries" */ 'core-js/features/object/entries'
  ),
}

const conds = {
  from: [].from,
  find: [].find,
  findIndex: [].findIndex,
  includes: [].includes,
  assign: Object.assign,
  values: Object.values,
  // keys: Object.keys,
  entries: Object.entries,
  symbol: typeof Symbol !== 'function',
}

const usedInProject = [
  // 'keys',
  // 'find',
  // 'values',
  // 'from',
  // 'includes',
  // 'entries'
]

const promises = usedInProject.reduce((acc, key) => {
  return conds[key] ? acc : [...acc, polyfills[key]]
}, [])

export default Promise.all(promises)
