function sum (a) {
    let _a = a;

    return b => {
        if (typeof _a === 'number' && typeof b === 'number') {
            return _a + b;
        } else {
            return NaN;
        }
    }
}

console.log(sum(2)(3))
console.log(sum(5)(4))
console.log(sum(1)(6))